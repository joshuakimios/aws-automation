# Automating AWS with Python

Repository for *automating aws with python*

## 01-webotron

Webotro is a script that will sync a local directory to an s3 bucket, and optionally configure Route 53 and cloudfront as well.

### Features

Webotron currently has the following features:

- List buckets
